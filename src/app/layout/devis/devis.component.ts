import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { StringifyOptions } from 'querystring';

@Component({
  selector: 'app-devis',
  templateUrl: './devis.component.html',
  styleUrls: ['./devis.component.scss']
})
export class DevisComponent implements OnInit {
  isLinear = false;
  formGroup1: FormGroup;
  formGroup2: FormGroup;
  formGroup3: FormGroup;

  choice = "Vous-même ?";
  souscriptions: string[] = ["Vous-même ?", "Vous et dʼautres personnes ?", "Dʼautres personnes uniquement ?"];

  // select nb adultes et enfants
  firstSelectedAdulte = "1e";
  firstSelectedenfant = "0e";
  secondSelectedAdulte = "0e";
  secondSelectedenfant = "0e";

  // checkbox
  //checked1 = false;
  //checked2 = true;

  // choix oui / non 
  choixON: string[] = ["Oui", "Non"];
  choiceON = "Non";

  // les champs à mémoriser pour l'utiliser dans 2éme stepper
  prenomText: string;
  nomUsageText: string;
  dateNaissanceText: string;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  telephoneMobileFormControl = new FormControl('', [
    Validators.required
  ]);

  prenomFormControl = new FormControl('', [
    Validators.required
  ]);

  civiliteFormControl = new FormControl('', [
    Validators.required
  ]);

  usageFormControl = new FormControl('', [
    Validators.required
  ]);

  adresseFormControl = new FormControl('', [
    Validators.required
  ]);

  codePostalFormControl = new FormControl('', [
    Validators.required
  ]);

  paysFormControl = new FormControl('', [
    Validators.required
  ]);

  devis ={
    prenom: ""
  };

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.formGroup1 = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.formGroup2 = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.formGroup3 = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
  }
}
